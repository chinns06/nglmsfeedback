﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CustomerManager.Controllers
{
    public class ContactUsController : ApiController
    {
        public ContactUsController()
        {

        }

        [HttpGet]
        [Queryable]
        public HttpResponseMessage getContactDetails()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Contact Details");
        }
    }
}