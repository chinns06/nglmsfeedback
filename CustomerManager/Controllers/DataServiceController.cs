﻿using CustomerManager.Framework;
using CustomerManager.Model;
using CustomerManager.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Security;

namespace CustomerManager.Controllers
{
    public class DataServiceController : ApiController
    {
        UserRepository _UserRepository;

        public DataServiceController()
        {
            _UserRepository = new UserRepository();
        }

      

        [HttpPost]
        public HttpResponseMessage Login(UserLogin userLogin)        
        {

            var Users = _UserRepository.GetUser(userLogin.UserName);
            ////var Users = _UserRepository.GetUser("sgankum");

            if (Users != null && Users.UserID == userLogin.UserName && Users.Password == userLogin.Password)
            {
                LMSCookie.Store(Users);
                string EncryptedUserName = Crypto.EncryptStringAES(userLogin.UserName, "nextGenLmsFeedbackPortalAccess");
                return Request.CreateResponse(HttpStatusCode.OK, new { status = true, UserName = EncryptedUserName, FirstName = Users.FirstName, LastName = Users.LastName });
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, new { status = false });

            }
            //Simulated login
            
        }

        [HttpPost]
        public HttpResponseMessage Logout()
        {
            LMSCookie.ClearCookie();
            //Simulated logout
            return Request.CreateResponse(HttpStatusCode.OK, new { status = true });
        }      
    }
}