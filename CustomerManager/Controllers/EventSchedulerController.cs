﻿using CustomerManager.Framework;
using CustomerManager.Model;
using CustomerManager.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CustomerManager.Controllers
{
    public class EventSchedulerController : ApiController
    {
        EvemtsRepository eventRepository;

        public EventSchedulerController()
        {
            eventRepository = new EvemtsRepository();
        }

        [HttpGet]
        [Queryable]
        [SessionAuthorize]
        public HttpResponseMessage getEvents()
        {
            List<EventsViewModel> events = new List<EventsViewModel>();
            var eventList = eventRepository.GetEventList();

            for (int counter = 0; counter <= eventList.Count - 1; counter++)
            {
                EventsViewModel eventdetails = new EventsViewModel();
                eventdetails.allDay = false;
                eventdetails.endTime = eventList[counter].EndTime != null ? Convert.ToDateTime(eventList[counter].EndTime).ToString("s") : string.Empty;
                eventdetails.startTime = eventList[counter].StartTime != null ? Convert.ToDateTime(eventList[counter].StartTime).ToString("s") : string.Empty;
                eventdetails.title = eventList[counter].Name;
                events.Add(eventdetails);
            }
            return Request.CreateResponse(HttpStatusCode.OK, events);
        }
    }
}