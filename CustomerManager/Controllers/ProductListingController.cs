﻿using CustomerManager.Framework;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CustomerManager.Controllers
{
    public class ProductListingController : ApiController
    {
        public ProductListingController()
        {

        }

        [HttpGet]
        [Queryable]
        [SessionAuthorize]
        public HttpResponseMessage getProductListing()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Products");
        }
    }
}