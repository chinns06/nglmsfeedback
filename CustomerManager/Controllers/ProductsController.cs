﻿using CustomerManager.Framework;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CustomerManager.Controllers
{
    public class ProductsController : ApiController
    {
        public ProductsController()
        {

        }

        [HttpGet]
        [Queryable]
        [SessionAuthorize]
        public HttpResponseMessage getProducts()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Products");
        }
    }
}