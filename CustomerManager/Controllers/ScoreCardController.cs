﻿using CustomerManager.Framework;
using CustomerManager.Model;
using CustomerManager.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CustomerManager.Controllers
{
    public class ScoreCardController : ApiController
    {
        QuestionsRepository _Repository;
        UserRatingsRepository _UserRatingsRepository;
        QuestionProductMappingRepository _QuestionProductMappingRepository;
        public ScoreCardController()
        {
            _Repository = new QuestionsRepository();
            _UserRatingsRepository = new UserRatingsRepository();
            _QuestionProductMappingRepository = new QuestionProductMappingRepository();
        }

        [HttpPost]
        [Queryable]
        [SessionAuthorize]
        public HttpResponseMessage getQuestions([FromBody]int productId)
        {
            //TODO: it has to be retrieved from parameter
            //var productId = 1;
            //give the roleid from session
            var questionlist = _Repository.GetQuestions("").ToList();
            var questionProductMapping = FetchQuestionProductMapping(productId).ToList();

            // Left join Questions and Questionproductmapping to get the usecasetype(pdf/video) and usecase path
            var result =
                from q in questionlist
                join qP in questionProductMapping
                    on q.Id equals qP.QuestionId into temp
                    from j in temp.DefaultIfEmpty()
                select new QuestionsViewModel
                {
                    Id = q.Id,
                    Name = q.Name,
                    Description = q.Description,
                    Rating =q.Rating,
                    Comments = q.Comments,
                    ProductId = j==null ? 0: j.ProductId,
                    UsercaseType = j == null ? "pdf" : j.UsercaseType,
                    UsecasePath = j == null ?string.Empty : j.UsecasePath
                };

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private IQueryable<QuestionProductMapping> FetchQuestionProductMapping(int productId)
        {
            return _QuestionProductMappingRepository.FetchAll(productId);
        }


        public HttpResponseMessage SaveRating([FromBody]IList<QuestionsViewModel> questionViewModel)
        {

            Model.Users UserProfile = LMSCookie.Restore();

            questionViewModel.ToList().ForEach( x => { x.UserID = UserProfile.Id; x.GroupId = 1; });

            IList<UserRatings> userRatings = (from v in questionViewModel.ToList()
                                         select new UserRatings
                                         {
                                                    UserId=v.UserID,
                                                    GroupID=v.GroupId,
                                                    QuestionId = v.Id,
                                                    Rating=v.Rating,
                                                    Comments =v.Comments,
                                            
                                         }).ToList();

            _UserRatingsRepository.SaveOrUpdate(userRatings);

            return Request.CreateResponse(HttpStatusCode.Created, questionViewModel);
        }

        public HttpResponseMessage SaveRow([FromBody]QuestionsViewModel questionViewModel)
        {
            Model.Users UserProfile = LMSCookie.Restore();
            questionViewModel.UserID = UserProfile.Id;
            questionViewModel.GroupId = 1;

            UserRatings userRating = new UserRatings
            {
                UserId = questionViewModel.UserID,
                GroupID = questionViewModel.GroupId,
                QuestionId = questionViewModel.Id,
                Rating = questionViewModel.Rating,
                Comments = questionViewModel.Comments
            };

            _UserRatingsRepository.SaveOrUpdate(userRating);

            return Request.CreateResponse(HttpStatusCode.Created, questionViewModel);
        }
    }
}