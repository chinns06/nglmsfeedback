﻿using CustomerManager.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace CustomerManager.Framework
{
    [AttributeUsage(AttributeTargets.All)]
    public class SessionAuthorizeAttribute : AuthorizeAttribute
    {
        UserRepository _UserRepository;

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            string requestAuthorizationHeader = HttpContext.Current.Request.Headers["Authorization"];
            if (!string.IsNullOrEmpty(requestAuthorizationHeader) && requestAuthorizationHeader != "null")
            {
                var decryptedUserName = Crypto.DecryptStringAES(requestAuthorizationHeader, "nextGenLmsFeedbackPortalAccess");
                Model.Users UserProfile = LMSCookie.Restore();
                return UserProfile == null ? false : decryptedUserName == UserProfile.UserID ? true : false;
            }
            else
                return false;
        }

    }
}