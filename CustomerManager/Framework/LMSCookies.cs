﻿using CustomerManager.Model;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace CustomerManager.Framework
{
    public static class LMSCookie
    {
        public static void Store(Users UserProfile)
        {
            HttpCookie cookie = new HttpCookie("LMSCookie")
            {
                // Set the expiry date of the cookie to 15 years
                Expires = DateTime.Now.AddHours(120)
            };
            Stream myStream = new MemoryStream();
            try
            {
                // Create a binary formatter and serialize the
                // myClass into the memorystream
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(myStream, UserProfile);
                // Go to the beginning of the stream and
                // fill a byte array with the contents of the
                // memory stream
                myStream.Seek(0, SeekOrigin.Begin);
                byte[] buffer = new byte[myStream.Length];
                myStream.Read(buffer, 0, (int)myStream.Length);
                // Store the buffer as a base64 string in the cookie
                cookie.Value = Convert.ToBase64String(buffer);
                // Add the cookie to the current http context
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            finally
            {
                // ... and remember to close the stream
                myStream.Close();
            }
        }

        public static Users Restore()
        {
            // Always remember to check that the cookie is not empty
            HttpCookie cookie = HttpContext.Current.Request.Cookies["LMSCookie"];
            if (cookie != null)
            {
                // Convert the base64 string into a byte array
                byte[] buffer = Convert.FromBase64String(cookie.Value);
                // Create a memory stream from the byte array
                Stream myStream = new MemoryStream(buffer);
                try
                {
                    // Create a binary formatter and deserialize the
                    // contents of the memory stream into MyClass
                    IFormatter formatter = new BinaryFormatter();
                    Users streamedClass = (Users)formatter.Deserialize(myStream);
                    return streamedClass;
                }
                finally
                {
                    // ... and as always, close the stream
                    myStream.Close();
                }
            }
            return null;
        }

        public static void ClearCookie()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["LMSCookie"];
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
            HttpContext.Current.Session.Remove("LMSCookie");
        }

    }
}