﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerManager.Model
{
    public class QuestionsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int UserID { get; set; }
        public int QuestionID { get; set; }
        public int Rating { get; set; }

        public int GroupId { get; set; }

        public string Comments { get; set; }

        public int ProductId { get; set; }

        public string UsercaseType { get; set; }

        public string UsecasePath { get; set; }
    }
}