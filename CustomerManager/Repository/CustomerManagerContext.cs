﻿using CustomerManager.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CustomerManager.Repository
{
    public class CustomerManagerContext : DbContext
    {
        public CustomerManagerContext(string connectionString)
            : base(connectionString)
        {
            //Configuration.ProxyCreationEnabled = false;
            //Configuration.LazyLoadingEnabled = true;
        }

        // DEVELOPMENT ONLY: initialize the database
        static CustomerManagerContext()
        {
           Database.SetInitializer<CustomerManagerContext>(null);
        }


        public virtual DbSet<Events> Events { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }        
        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<UserGroupsMapping> UserGroupsMapping { get; set; }
        public virtual DbSet<UserRolesMapping> UserRolesMapping { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Questions> Questions { get; set; }
        public virtual DbSet<QuestionRolesMapping> QuestionsRolesMapping { get; set; }

        public virtual DbSet<QuestionProductMapping> QuestionProductMapping { get; set; }
        public virtual DbSet<UserRatings> UserRatings { get; set; }

    }
}