﻿using CustomerManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace CustomerManager.Repository
{
    public class EvemtsRepository
    {
        CustomerManagerContext _Context;

        //string connectionString = ConfigurationManager.ConnectionStrings["CustomerManagerContext"].ConnectionString;
        string connectionString = Helper.GetRDSConnectionString();
        public EvemtsRepository()
        {
            _Context = new CustomerManagerContext(connectionString);
        }

        public List<Events> GetEventList()
        {
            return _Context.Events.ToList();
        }

    }
}