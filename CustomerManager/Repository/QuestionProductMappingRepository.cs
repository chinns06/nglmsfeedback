﻿using CustomerManager.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerManager.Repository
{
    public class QuestionProductMappingRepository
    {
        CustomerManagerContext _Context;

        string connectionString = Helper.GetRDSConnectionString();

        public QuestionProductMappingRepository()
        {
            _Context = new CustomerManagerContext(connectionString);
            //System.Threading.Thread.Sleep(5000); 
        }

        public IQueryable<QuestionProductMapping> FetchAll(int productId)
        {
            return _Context.QuestionProductMapping.Where(x => x.ProductId == productId);

        }


    }
}