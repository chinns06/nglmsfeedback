﻿using CustomerManager.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CustomerManager.Repository
{
    public class QuestionsRepository
    {
        CustomerManagerContext _Context;

        //string connectionString = ConfigurationManager.ConnectionStrings["CustomerManagerContext"].ConnectionString;
        string connectionString = Helper.GetRDSConnectionString();
        public QuestionsRepository()
        {
            _Context = new CustomerManagerContext(connectionString);
            //System.Threading.Thread.Sleep(5000); 
        }

        public IQueryable<QuestionsViewModel> GetQuestions(string userName)
        {
           // var query = _Context.Questions;

            return
                (from q in _Context.Questions
                join ur in _Context.UserRatings
                    on q.Id equals ur.QuestionId into temp
                from j in temp.DefaultIfEmpty()
                select new QuestionsViewModel
                {
                    Id = q.Id,
                    Name = q.Name,
                    Description = q.Description,
                    Rating = j==null ? 0:j.Rating,
                    Comments = j==null ? string.Empty:j.Comments
                });           

        } 

    }
}