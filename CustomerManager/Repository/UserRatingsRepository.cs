﻿using CustomerManager.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CustomerManager.Repository
{
    public class UserRatingsRepository
    {
        CustomerManagerContext _Context;
        //string connectionString = ConfigurationManager.ConnectionStrings["CustomerManagerContext"].ConnectionString;
        string connectionString = Helper.GetRDSConnectionString();


        public UserRatingsRepository()
        {
            _Context = new CustomerManagerContext(connectionString);
            //System.Threading.Thread.Sleep(5000); 
        }


        public void SaveOrUpdate(UserRatings entity)
        {
            var userRating = _Context.UserRatings.FirstOrDefault(e => e.UserId == entity.UserId
                                                      && e.QuestionId == entity.QuestionId
                                                      && e.GroupID == entity.GroupID);
            if (userRating == null)
            {
                entity.CreatedBy = entity.UserId.ToString();
                entity.CreatedOn = System.DateTime.Now;
                entity.ModifiedBy = entity.UserId.ToString();
                entity.ModifiedOn = System.DateTime.Now;
                _Context.UserRatings.Add(entity);
            }
            else
            {
                entity.Id = userRating.Id;
                entity.CreatedOn = userRating.CreatedOn;
                entity.ModifiedBy = entity.UserId.ToString();
                entity.ModifiedOn = System.DateTime.Now;

                _Context.Entry(userRating).CurrentValues.SetValues(entity);
            }

            _Context.SaveChanges();
        }

        public void SaveOrUpdate(IList<UserRatings> userRatings)
        {
            userRatings.ToList().ForEach(entity =>
            {
                var userRating = _Context.UserRatings.FirstOrDefault(e => e.UserId == entity.UserId
                                                       && e.QuestionId == entity.QuestionId
                                                       && e.GroupID == entity.GroupID);
                if (userRating == null)
                {
                    entity.CreatedBy = entity.UserId.ToString();
                    entity.CreatedOn = System.DateTime.Now;
                    entity.ModifiedBy = entity.UserId.ToString();
                    entity.ModifiedOn = System.DateTime.Now;
                    _Context.UserRatings.Add(entity);
                }
                else
                {
                    entity.Id= userRating.Id;
                    entity.CreatedOn = userRating.CreatedOn;
                    entity.ModifiedBy = entity.UserId.ToString();
                    entity.ModifiedOn = System.DateTime.Now;
                 
                    _Context.Entry(userRating).CurrentValues.SetValues(entity);
                }

            });

            _Context.SaveChanges();

        }

    }
}