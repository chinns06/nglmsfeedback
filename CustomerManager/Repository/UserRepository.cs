﻿using CustomerManager.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CustomerManager.Repository
{
    public class UserRepository
    {
        CustomerManagerContext _Context;
        //string connectionString = ConfigurationManager.ConnectionStrings["CustomerManagerContext"].ConnectionString;
        string connectionString = Helper.GetRDSConnectionString();
        
        public UserRepository()
        {
            _Context = new CustomerManagerContext(connectionString);
            //System.Threading.Thread.Sleep(5000); 
        }

        public Users GetUserById(int id)
        {
            return _Context.Users
                    .Include("UserGroupsMapping")
                    .Include("UserRolesMapping")
                    .SingleOrDefault(u => u.Id == id);
        }


        public Users GetUser(string userName)
        {
            return _Context.Users                    
                    .SingleOrDefault(u => u.UserID == userName);
        }

    }
}