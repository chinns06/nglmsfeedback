﻿require.config({
    baseUrl: 'app',
    urlArgs: 'v=1.0'
});

require(
    [
        'lmsApp/animations/listAnimations',
        'lmsApp/Calendar/calendar-tpls',
        'app',
        'lmsApp/directives/wcUnique',
        'lmsApp/services/routeResolver',
        'lmsApp/services/config',
        'lmsApp/services/authService',
        'lmsApp/services/lmsService',
        'lmsApp/services/dataService',
        'lmsApp/services/modalService',
        'lmsApp/services/httpInterceptors',
        'lmsApp/filters/nameCityStateFilter',
        'lmsApp/filters/nameProductFilter',
        'lmsApp/controllers/navbarController',
    ],
    function () {
        angular.bootstrap(document, ['lmsApp']);
    });
