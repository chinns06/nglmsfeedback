﻿'use strict';

define(['lmsApp/services/routeResolver'], function () {

    var app = angular.module('lmsApp', ['ngRoute', 'ngAnimate','ngMaterial', 'routeResolverServices',
                                              'wc.directives', 'wc.animations', 'ui.bootstrap', 'breeze.angular', 'ui.rCalendar']);

    app.config(['$routeProvider', 'routeResolverProvider', '$controllerProvider',
                '$compileProvider', '$filterProvider', '$provide', '$httpProvider',

        function ($routeProvider, routeResolverProvider, $controllerProvider,
                  $compileProvider, $filterProvider, $provide, $httpProvider) {

            //Change default views and controllers directory using the following:
            //routeResolverProvider.routeConfig.setBaseDirectories('/app/views', '/app/controllers');

            app.register =
            {
                controller: $controllerProvider.register,
                directive: $compileProvider.directive,
                filter: $filterProvider.register,
                factory: $provide.factory,
                service: $provide.service
            };

            //Define routes - controllers will be loaded dynamically
            var route = routeResolverProvider.route;

            $routeProvider
                //route.resolve() now accepts the convention to use (name of controller & view) as well as the 
                //path where the controller or view lives in the controllers or views folder if it's in a sub folder. 
                //For example, the controllers for customers live in controllers/customers and the views are in views/customers.
                //The controllers for orders live in controllers/orders and the views are in views/orders
                //The second parameter allows for putting related controllers/views into subfolders to better organize large projects
                //Thanks to Ton Yeung for the idea and contribution
                .when('/Products', route.resolve('Products', 'Products/', 'vm'))
                 .when('/ProductListing', route.resolve('ProductListing', 'ProductListing/', 'vm'))
                .when('/ScoreCard', route.resolve('ScoreCard', 'ScoreCard/', 'vm'))
                .when('/ScoreCard/:prdId', route.resolve('ScoreCardNew', 'ScoreCard/', 'vm'))
                 .when('/ContactUs', route.resolve('ContactUs', 'ContactUs/', 'vm'))
                .when('/EventScheduler', route.resolve('EventScheduler', 'EventScheduler/', 'vm'))
                .when('/login/:redirect*?', route.resolve('Login', '', 'vm'))
                .otherwise({ redirectTo: '/login/:redirect*?' });

    }]);

    app.run(['$rootScope', '$location', 'authService', '$http',
    function ($rootScope, $location, authService, $http) {
            
            //Client-side security. Server-side framework MUST add it's 
            //own security as well since client-based security is easily hacked
            $rootScope.$on("$routeChangeStart", function (event, next, current) {
                if (next && next.$$route && next.$$route.secure) {
                    if ($window.sessionStorage.token == null || $window.sessionStorage.token == "undefined") {
                            $rootScope.$evalAsync(function () {
                                authService.redirectToLogin();
                            });
                    }
                    else {
                    }
                }
            });

    }]);

    return app;

});





