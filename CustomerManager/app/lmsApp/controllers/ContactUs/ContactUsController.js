﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['$location', '$filter', '$window',
                        '$timeout', 'authService', 'dataService'];

    var ContactUsController = function ($location, $filter, $window,
        $timeout, authService, dataService) {

        var vm = this;

        function init() {
            dataService.getContactDetails();
            angular.element(document.querySelector("#header_logo")).addClass("show");
            angular.element(document.querySelector("#header_menu")).addClass("show");
            angular.element(document.querySelector("#header_logo")).removeClass("hide");
            angular.element(document.querySelector("#header_menu")).removeClass("hide");

            angular.element(document.querySelector("#loggedUserName")).text("Welcome " + $window.sessionStorage.userName + " ");

        }

        init();
    };

    ContactUsController.$inject = injectParams;

    app.register.controller('ContactUsController', ContactUsController);

});