﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['$location', '$filter', '$window',
                        '$timeout', 'authService', 'dataService', '$scope', '$sce'];

    var ProductListingController = function ($location, $filter, $window,
        $timeout, authService, dataService, $scope, $sce) {

        $scope.imagePath = 'img/washedout.png';

        function init() {
            dataService.getProductListing();
            angular.element(document.querySelector("#header_logo")).addClass("show");
            angular.element(document.querySelector("#header_menu")).addClass("show");
            angular.element(document.querySelector("#header_logo")).removeClass("hide");
            angular.element(document.querySelector("#header_menu")).removeClass("hide");

            angular.element(document.querySelector("#loggedUserName")).text("Welcome " + $window.sessionStorage.userName + " ");

            $scope.rate = 7;
            $scope.max = 10;
            $scope.isReadonly = true;
        }    

        init();
    };

    angular.module('MyApp', ['ngMaterial'])
        .config(['$mdIconProvider', function ($mdIconProvider) {
            $mdThemingProvider.theme('dark-grey').backgroundPalette('grey').dark();
            $mdThemingProvider.theme('dark-orange').backgroundPalette('orange').dark();
            $mdThemingProvider.theme('dark-purple').backgroundPalette('deep-purple').dark();
            $mdThemingProvider.theme('dark-blue').backgroundPalette('blue').dark();
        }]);

 

    ProductListingController.$inject = injectParams;
    app.register.controller('ProductListingController', ProductListingController);
});