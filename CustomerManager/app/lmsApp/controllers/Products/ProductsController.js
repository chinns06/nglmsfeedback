﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['$location', '$filter', '$window',
                        '$timeout', 'authService', 'dataService', '$scope', '$sce'];

    var ProductsController = function ($location, $filter, $window,
        $timeout, authService, dataService, $scope, $sce) {

        var vm = this;


        function init() {
            dataService.getProducs();
            $scope.url = $sce.trustAsResourceUrl("Content/PDF/LMS_Use_Cases.pdf");
            angular.element(document.querySelector("#header_logo")).addClass("show");
            angular.element(document.querySelector("#header_menu")).addClass("show");
            angular.element(document.querySelector("#header_logo")).removeClass("hide");
            angular.element(document.querySelector("#header_menu")).removeClass("hide");

            angular.element(document.querySelector("#loggedUserName")).text("Welcome " + $window.sessionStorage.userName + " ");

        }

        $scope.changeIt = function (file) {
            $scope.url = $sce.trustAsResourceUrl(file);
        }

        init();
    };


    ProductsController.$inject = injectParams;


    app.register.controller('ProductsController', ProductsController);
    angular.module('prodApp', [])
});