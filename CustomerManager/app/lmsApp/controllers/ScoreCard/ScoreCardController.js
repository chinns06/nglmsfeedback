﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['$location', '$filter', '$window',
                        '$timeout', 'authService', 'dataService'];

    var ScoreCardController = function ($location, $filter, $window,
        $timeout, authService, dataService) {

        var vm = this;

        vm.saveRating = function () {
            //if ($scope.editForm.$valid) {
            debugger;
            dataService.saveRating(vm.questionlist).then(processSuccess, processError);
               
            //}
        };

        function init() {
            initializeRating();

            dataService.getQuestions()
             .then(function (data) {
                 debugger;
                 vm.questionlist = data.results;                

                 $timeout(function () {
                     vm.cardAnimationClass = ''; //Turn off animation since it won't keep up with filtering
                 }, 1000);

             }, function (error) {
                 $window.alert('Sorry, an error occurred: ' + error.data.message);
             });
        }

        function initializeRating() {
            
            app.register.controller('RatingDemoCtrl', ['$scope', function ($scope) {
                $scope.rate = 1;
                $scope.max = 10;
                $scope.isReadonly = false;

                $scope.hoveringOver = function (value) {
                    $scope.overStar = value;
                    $scope.percent = 100 * (value / $scope.max);
                };

                $scope.ratingStates = [
                  { stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle' },
                  { stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty' },
                  { stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle' },
                  { stateOn: 'glyphicon-heart' },
                  { stateOff: 'glyphicon-off' }
                ];
            }]);

            app.register.controller('modalPopupCtrl', function ($scope, $mdDialog) {
                $scope.status = '  ';
                $scope.customFullscreen = false;

                $scope.showAlert = function (ev) {
                    debugger;
                    var aa = vm;
                    // Appending dialog to document.body to cover sidenav in docs app
                    // Modal dialogs should fully cover application
                    // to prevent interaction outside of dialog
                    $mdDialog.show(
                      $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('This is an alert title')
                        .textContent('<md-dialog>' +
                                    '  <md-dialog-content>' +
                                    '     Welcome to TutorialsPoint.com' +
                                    '  </md-dialog-content>' +
                                    '</md-dialog>')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Got it!')
                        .targetEvent(ev)
                    );
                };

                $scope.showCustom = function (event,q) {
                    debugger;
                    var qdescr = q.description;
                    if (q.description == null)
                        qdescr = '';
                    var dialogContent = '<md-dialog>' +
                                    '<h2>' + q.name + '</h2>' +
                                    '  <md-dialog-content>' +
                                    qdescr +
                                    '  </md-dialog-content>' +
                                    '</md-dialog>'
                    $mdDialog.show({
                        clickOutsideToClose: true,
                        scope: $scope,
                        preserveScope: true,
                        template: dialogContent,
                        controller: function DialogController($scope, $mdDialog) {
                            $scope.closeDialog = function () {
                                $mdDialog.hide();
                            }
                        }
                    });
                };

                //this.topDirections = ['left', 'up'];
                //this.bottomDirections = ['down', 'right'];

                //this.isOpen = false;

                //this.availableModes = ['md-fling', 'md-scale'];
                //this.selectedMode = 'md-fling';

                //this.availableDirections = ['up', 'down', 'left', 'right'];
                //this.selectedDirection = 'up';
            });
        }

        function processSuccess() {
            $scope.editForm.$dirty = false;
           // vm.updateStatus = true;
           // vm.title = 'Edit';
           // vm.buttonText = 'Update';
            startTimer();
        }

        function processError(error) {
            vm.errorMessage = error.message;
            startTimer();
        }

        function startTimer() {
            timer = $timeout(function () {
                $timeout.cancel(timer);
                vm.errorMessage = '';
                vm.updateStatus = false;
            }, 3000);
        }

        init();
    };

    ScoreCardController.$inject = injectParams;

    app.register.controller('ScoreCardController', ScoreCardController);

    angular.module('MyApp', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache']);
  // angular.module('ui.bootstrap.demo', ['ngAnimate', 'ui.bootstrap']);


});