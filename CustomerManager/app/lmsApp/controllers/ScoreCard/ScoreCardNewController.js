﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['$location', '$routeParams', '$filter', '$window',
                        '$timeout', 'authService', 'dataService'];

    var ScoreCardNewController = function ($location, $routeParams, $filter, $window,
        $timeout, authService, dataService) {

        debugger;
        var vm = this, productId = $routeParams.prdId, timer;

        vm.saveRating = function () {
            //if ($scope.editForm.$valid) {
            // debugger;
            dataService.saveRating(vm.questionlist).then(processSuccess, processError);

            //}
        };

        vm.saveRow = function (obj) {
            //if ($scope.editForm.$valid) {
            debugger;
            dataService.saveRow(obj.question).then(processSuccess, processError);

            //}
        };

        function init() {

            dataService.getProductListing();
            angular.element(document.querySelector("#header_logo")).addClass("show");
            angular.element(document.querySelector("#header_menu")).addClass("show");
            angular.element(document.querySelector("#header_logo")).removeClass("hide");
            angular.element(document.querySelector("#header_menu")).removeClass("hide");

            angular.element(document.querySelector("#loggedUserName")).text("Welcome " + $window.sessionStorage.userName + " ");

            initializeRating();

            dataService.getQuestions(productId)
             .then(function (data) {
                 debugger;
                 vm.questionlist = data.results;
                 vm.productId = productId;

                 $timeout(function () {
                     vm.cardAnimationClass = ''; //Turn off animation since it won't keep up with filtering
                 }, 1000);

             }, function (error) {
                 $window.alert('Sorry, an error occurred: ' + error.data.message);
             });
        }

        function initializeRating() {

            app.register.controller('RatingDemoCtrl', function ($scope) {
                $scope.rate = 1;
                $scope.max = 10;
                $scope.isReadonly = false;

                $scope.hoveringOver = function (value) {
                    $scope.overStar = value;
                    $scope.percent = 100 * (value / $scope.max);
                };

                $scope.ratingStates = [
                  { stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle' },
                  { stateOn: 'glyphicon-star', stateOff: 'glyphicon-star-empty' },
                  { stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle' },
                  { stateOn: 'glyphicon-heart' },
                  { stateOff: 'glyphicon-off' }
                ];
            })
            

            app.register.controller('modalPopupCtrl', function ($scope, $mdDialog) {
                $scope.status = '  ';
                $scope.customFullscreen = false;

                $scope.showCustom = function (event, q) {
                    //debugger;
                    var qdescr = q.description;
                    if (q.description == null)
                        qdescr = 'Not Applicable';

                    var dialogContent = ' <div class="md-panel demo-dialog-example" tabindex="-1">' +
                          '<div role="dialog"  layout="column" layout-align="center center" class="layout-align-center-center layout-column">' +
                              '<md-toolbar class="_md _md-toolbar-transitions">' +
                                '<div class="md-toolbar-tools">' +
                                  '<h2>' + q.name + ' - Evaluation Criteria </h2>' +
                                '</div>' +
                              '</md-toolbar>' +

                             ' <div class="demo-dialog-content" style="padding-left:10px">' +
                                '<p>' +
                                 qdescr +
                               ' </p>' +

                              '<div layout="row" class="demo-dialog-button layout-row">' +
                               ' <button class="md-primary md-button md-autofocus md-ink-ripple flex" type="button"   flex="" ng-click="closeDialog()"><span class="ng-scope">' +
                                '    Close' +
                               ' </span></button>' +
                              '</div>' +
                        '</div>' +
                   ' </div>'

                    //var dialogContent = '<md-dialog>' +
                    //                '<h2>' + q.name + '</h2>' +
                    //                '  <md-dialog-content>' +
                    //                qdescr +
                    //                '  </md-dialog-content>' +
                    //                '</md-dialog>'
                    $mdDialog.show({
                        clickOutsideToClose: true,
                        scope: $scope,
                        preserveScope: true,
                        template: dialogContent,
                        controller: function DialogController($scope, $mdDialog) {
                            $scope.closeDialog = function () {
                                $mdDialog.hide();
                            }
                        }
                    });
                };

                // load PRODUCTS
                this.userProduct = '';
                this.products = ('SABA CornerStone SumTotal').split(' ').map(function (product) { return { abbrev: product }; });
            });


        }

        function processSuccess() {
            //$scope.editForm.$dirty = false;
             vm.updateStatus = true;
            // vm.title = 'Edit';
            // vm.buttonText = 'Update';
            //startTimer();
        }

        function processError(error) {
            vm.errorMessage = error.message;
            startTimer();
        }

        function startTimer() {
            timer = $timeout(function () {
                $timeout.cancel(timer);
                vm.errorMessage = '';
                vm.updateStatus = false;
            }, 3000);
        }

        init();
    };

    ScoreCardNewController.$inject = injectParams;

    app.register.controller('ScoreCardNewController', ScoreCardNewController);

 });