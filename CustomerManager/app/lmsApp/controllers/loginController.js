﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['$location', '$routeParams', 'authService', '$scope', '$mdDialog'];

    var LoginController = function ($location, $routeParams, authService, $scope, $mdDialog) {
        var vm = this,
            path = '/';

        vm.email = null;
        vm.username = null;
        vm.password = null;
        vm.errorMessage = null;

        $scope.UserName = '';

        angular.element(document.querySelector("#header_logo")).addClass("hide");
        angular.element(document.querySelector("#header_menu")).addClass("hide");
        angular.element(document.querySelector("#header_logo")).removeClass("show");
        angular.element(document.querySelector("#header_menu")).removeClass("show");

        angular.element(document.querySelector("body")).addClass("login-background");

        $scope.onBlur = function ($event) {
            if (angular.element(document.querySelector('#UserName')).val() == "")
                angular.element(document.querySelector("#UserName")).removeClass("used");
            else
                angular.element(document.querySelector("#UserName")).addClass("used");

            if (angular.element(document.querySelector('#password')).val() == "")
                angular.element(document.querySelector("#password")).removeClass("used");
            else
                angular.element(document.querySelector("#password")).addClass("used");
        }

        vm.login = function () {

            if (vm.username == null || vm.username == "undefined" || vm.username == "" || vm.password == null || vm.password == "undefined" || vm.password == "")
                return;

            authService.login(vm).then(function (status) {
                //$routeParams.redirect will have the route
                //they were trying to go to initially
                if (!status) {
                    // vm.errorMessage = 'Unable to login';



                    return;
                }

                if (status && $routeParams && $routeParams.redirect) {
                    path = '/ProductListing';
                }

                $location.path(path);
            });
        };

  

    };

    LoginController.$inject = injectParams;

    angular.module('dialogDemo2', ['ngMaterial'])

    app.register.controller('LoginController', LoginController);

});