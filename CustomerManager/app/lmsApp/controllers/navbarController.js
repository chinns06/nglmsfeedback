﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['$scope', '$location', 'config', 'authService', '$window'];

    var NavbarController = function ($scope, $location, config, authService, $window) {
        var vm = this,
            appTitle = 'NextGEN LMS', isUserAuthenticated ;

        vm.isCollapsed = false;
        vm.appTitle = (config.useBreeze) ? appTitle + ' Breeze' : appTitle;
        vm.isUserAuthenticated = $window.sessionStorage.token != null || $window.sessionStorage.token != "undefined";

        vm.highlight = function (path) {
            return $location.path().substr(0, path.length) === path;
        };

        vm.loginOrOut = function () {
            setLoginLogoutText();
            if (vm.isUserAuthenticated) { //logout 
                $window.sessionStorage.token = null;
                authService.logout().then(function () {
                    //$location.path('/');
                    //return;
                    redirectToLogin();
                });
            }
           // redirectToLogin();
        };

        vm.redirectProductListing = function () {
            $location.replace();
            $location.path('/ProductListing');
        };

        vm.redirectUseCase = function () {
            $location.replace();
            $location.path('/Products');
        };

        vm.scoreCard = function () {
            $location.replace();
            $location.path('/ScoreCard');
        };

        vm.eventScheduler = function () {
            $location.replace();
            $location.path('/EventScheduler');
        };

        vm.redirectContactPage = function () {
            $location.replace();
            $location.path('/ContactUs');
        };


        function redirectToLogin() {
            var path = '/login' + $location.$$path;
            $location.replace();
            $location.path(path);
        }

        $scope.$on('loginStatusChanged', function (loggedIn) {
            setLoginLogoutText(loggedIn);
        });

        $scope.$on('redirectToLogin', function () {
            redirectToLogin();
        });

        function setLoginLogoutText() {
            vm.loginLogoutText = (vm.isUserAuthenticated) ? 'Logout' : 'Login';
        }

        setLoginLogoutText();

    };

    NavbarController.$inject = injectParams;


    //Loaded normally since the script is loaded upfront 
    //Dynamically loaded controller use app.register.controller
    app.controller('NavbarController', NavbarController);

  ;

});