﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['$http', '$rootScope','$window'];

    var authFactory = function ($http, $rootScope, $window) {
        var serviceBase = '/api/dataservice/',
            factory = {
                loginPath: '/login',
                user: {
                    isAuthenticated: false,
                    roles: null
                }
            };

        factory.login = function (userLogin) {
            return $http.post(serviceBase + 'login', userLogin).then(
                function (results) {
                    var loggedIn = results.data.status;;
                    $window.sessionStorage.token = results.data.userName;
                    $window.sessionStorage.userName =  results.data.firstName + " " + results.data.lastName;
                    changeAuth(loggedIn);
                    return loggedIn;
                });
        };

        factory.logout = function () {
            return $http.post(serviceBase + 'logout').then(
                function (results) {
                    var loggedIn = !results.data.status;
                    changeAuth(loggedIn);
                    return loggedIn;
                });
        };

        factory.redirectToLogin = function () {
            $rootScope.$broadcast('redirectToLogin', null);
        };

        function changeAuth(loggedIn) {
            factory.user.isAuthenticated = loggedIn;
            $rootScope.$broadcast('loginStatusChanged', loggedIn);
        }

        return factory;
    };

    authFactory.$inject = injectParams;

    app.factory('authService', authFactory);

});
