﻿'use strict';

define(['app', 'lmsApp/services/lmsService'], function (app) {

    var injectParams = ['config', 'lmsService'];

    var dataService = function (config, lmsService) {
        return lmsService;
    };

    dataService.$inject = injectParams;

    app.factory('dataService',
        ['config', 'lmsService', dataService]);

});

