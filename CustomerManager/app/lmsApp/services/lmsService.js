﻿'use strict';

define(['app'], function (app) {

    var injectParams = ['$http', '$q'];

    var customersFactory = function ($http, $q) {
        var ProductserviceBase = '/api/Products/', ScoreCardServiceBase = '/api/ScoreCard/', ContactUsServiceBase = '/api/ContactUs/', EventSchedulerServiceBase = '/api/EventScheduler/', ProductListingServiceBase = '/api/ProductListing/',
            factory = {};

        factory.getProducs = function () {
            return $http.get(ProductserviceBase + 'getProducts').then(function (response) {
               // alert(response);
            });
        };

        factory.getQuestions = function (productId) {
            debugger;
            return $http.post(ScoreCardServiceBase + 'getQuestions', productId).then(function (response) {
                var questions = response.data;
                return {
                    results: questions
                };
            });
        };


        factory.saveRating = function (questionlist) {
            debugger;
            return $http.post(ScoreCardServiceBase + 'saveRating', questionlist).then(function (results) {
               
                return results;
            });
           
        };

        factory.saveRow = function (question) {
            debugger;
            return $http.post(ScoreCardServiceBase + 'saveRow', question).then(function (results) {

                return results;
            });

        };

        factory.getContactDetails = function () {
            return $http.get(ContactUsServiceBase + 'getContactDetails').then(function (response) {
                // alert(response);
            });
        };

        factory.getEvents = function () {

        };

        factory.getProductListing = function () {
            return $http.get(ProductListingServiceBase + 'getProductListing').then(function (response) {
                // alert(response);
            });
        };
     
        return factory;
    };

  
    customersFactory.$inject = injectParams;

    app.factory('lmsService', customersFactory);

});